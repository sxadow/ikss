package application;

public class Message {
	public static class InvalidObjectName extends Exception {
		public InvalidObjectName(String string) {
			super(string);
		}
	}

	private final int messageSize;
	private final String[] destination;

	public static String[] parseString(String source) throws InvalidObjectName {
		// 3 - length of string destination "АП3"
		if (source.length() < 3) {
			throw new InvalidObjectName("Not valid object name: " + source);
		}
		int size = (int) Math.floor(source.length() / 3.0); // detect number of destinations 
		String[] s = new String[size];
		String tmp;
		int count = 0;
		for (int i = 0; i < source.length(); i++) {
			tmp = source.substring(i, i + 3);

			if (ValidNames.isValidObjectName(tmp)) {
				s[count] = new String(tmp);
				count++;
			} else if (i == 0) {
				throw new InvalidObjectName("Not valid object name: " + source);
			}

			if (count == size) {
				break;
			}
		}
		return s;
	}

	/**
	 * 
	 * @param messageSize
	 *            size in byte's
	 * @param destination
	 *            message destination for example ("АП3, АП4")
	 * @throws InvalidObjectName
	 * @throws Exception
	 *             when message or destination not valid
	 */
	public Message(int messageSize, String destination) throws InvalidObjectName {
		if (messageSize > 0) {
			this.messageSize = messageSize;
		} else {
			throw new IllegalArgumentException("Message size must be greater than ZERO");
		}
		this.destination = parseString(destination);
	}

	/**
	 * Get integer value of destinations
	 * 
	 * @return array of destinations | for example: for "АП1, АП3" it will
	 *         returns [1,3]
	 */
	public int[] getNumericDestination() {
		int num[] = new int[destination.length];
		for (int i = 0; i < num.length; i++) {
			num[i] = Integer.valueOf(destination[i].substring(destination[i].length() - 1));
		}
		return num;
	}

	public int getMessageSize() {
		return messageSize;
	}

	/**
	 * 
	 * Get String destinations
	 * 
	 * @return get array of destinations
	 */
	public String[] getDestination() {
		return destination;
	}

	public static void main(String[] args) throws Exception {
		Message msg = new Message(63, "АП3, АП4");
		//System.out.println(Message.parseString("АП9"));
		for (int a : msg.getNumericDestination()) {
			System.out.println(a);
		}
		for (String s : Message.parseString("АП3, АП4")) {
			System.out.println(s);
		}
	}
}
