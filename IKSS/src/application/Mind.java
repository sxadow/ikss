package application;

import java.awt.EventQueue;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import jxl.write.WriteException;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.Color;
import java.awt.Cursor;

public class Mind extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3309688038193034404L;
	// Heart
	String fileOpenName, fileSaveName;
	private Tasker tsk;
	private boolean isError = false;
	// UI
	private JPanel contentPane;
	private JButton btnFileChooser, btnWork;
	private JFileChooser fileChooser = new JFileChooser();

	/** Choose button tasker */
	public class LoadFileAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int returnVal = fileChooser.showOpenDialog(getParent());
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				fileOpenName = fileChooser.getSelectedFile().getAbsolutePath();
				fileChooser.setCurrentDirectory(new File(fileChooser.getCurrentDirectory().getAbsolutePath()));
				Mind.this.setTitle(fileOpenName);
				makeActionColor();
				isError = false;
				try {
					tsk = new Tasker(fileOpenName);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "Ошибка открытия файла:", JOptionPane.ERROR_MESSAGE);
					isError = true;
					e1.printStackTrace();
				}
			} else {
				JOptionPane.showMessageDialog(null, "Файл не выбран", "No file", JOptionPane.WARNING_MESSAGE);
			}
		}
	}

	private void Work() {
		long b = 0;
		long a = 0;
		try {
			a = System.currentTimeMillis();
			tsk.calculate();
			b = System.currentTimeMillis();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Ошибка при расчетах\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			isError = true;
			e.printStackTrace();
		}
		if (!isError) {
			JOptionPane.showMessageDialog(null, "Работа выполнена выберите место сохранения файла", "Время выполнения: " + (b - a) + " мсек",
					JOptionPane.INFORMATION_MESSAGE);
			isError = false;
		}
	}

	private void SaveFile() {
		boolean saveFileError = false;
		String extensions = "xls";
		fileChooser.setSelectedFile(new File(fileChooser.getSelectedFile().getName().substring(0, fileChooser.getSelectedFile().getName().length() - 4) + "m." + extensions));
		int returnVal = fileChooser.showSaveDialog(getParent());
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			fileSaveName = fileChooser.getSelectedFile().getAbsolutePath();
			if (!fileSaveName.substring(fileSaveName.length() - 4).equalsIgnoreCase("." + extensions)) {
				fileSaveName = fileSaveName + "m" + "." + extensions;
			}
			try {
				tsk.SaveTofile(fileSaveName);
				makeNeturalColor();
			} catch (IOException | WriteException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, "Ошибка при сохранении файла", "Error", JOptionPane.ERROR_MESSAGE);
				saveFileError = true;
				e.printStackTrace();
			}
			if (!saveFileError) {
				JOptionPane.showMessageDialog(null, "Файл успешно сохранен", ";)", JOptionPane.INFORMATION_MESSAGE);
				tsk.close();
				makeChooseColor();
			}
		}
	}

	private void makeChooseColor() {
		btnFileChooser.setForeground(Color.CYAN);
		btnWork.setForeground(Color.WHITE);
		btnWork.setEnabled(false);
	}

	public void makeActionColor() {
		btnFileChooser.setForeground(Color.WHITE);
		btnWork.setForeground(Color.CYAN);
		btnWork.setEnabled(true);
	}

	private void makeNeturalColor() {
		btnFileChooser.setForeground(Color.WHITE);
		btnWork.setForeground(Color.WHITE);
	}

	private class DoWorkAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			isError = false;
			Work();

			if (!isError) {
				SaveFile();
			} else {
				JOptionPane.showMessageDialog(null, "При работе с файлом возникла ошибка, откройте другой файл", "Error", JOptionPane.WARNING_MESSAGE);
				makeChooseColor();
			}
		}
	}

	// make Russian Locale
	private void initUIText() {
		setLocale(new Locale("ru", "UA"));
		UIManager.put("FileChooser.fileNameLabelText", "Имя файла:");
		UIManager.put("FileChooser.lookInLabelText", "Папка:");
		UIManager.put("FileChooser.Filename", "Имя файла:");
		UIManager.put("FileChooser.filesOfTypeLabelText", "Тип файла:");
		UIManager.put("FileChooser.cancelButtonText", "Отмена");
		UIManager.put("FileChooser.openButtonText", "Открыть");
		UIManager.put("FileChooser.openDialogTitleText", "Открыть");
		UIManager.put("FileChooser.lookInLabelText", "Папка");
		UIManager.put("FileChooser.fileNameLabelText", "Имя файла:");
		UIManager.put("FileChooser.openButtonToolTipText", "Открыть выделенные файлы");
		UIManager.put("FileChooser.cancelButtonToolTipText", "Отмена");
		UIManager.put("FileChooser.fileNameHeaderText", "Имя файла:");
		UIManager.put("FileChooser.upFolderToolTipText", "Вверх");
		UIManager.put("FileChooser.homeFolderToolTipText", "Рабочий стол");
		UIManager.put("FileChooser.newFolderToolTipText", "Новая папка");
		UIManager.put("FileChooser.listViewButtonToolTipText", "Список");
		UIManager.put("FileChooser.newFolderButtonText", "Создать новую папку");
		UIManager.put("FileChooser.renameFileButtonText", "Переименовать файл");
		UIManager.put("FileChooser.deleteFileButtonText", "Удалить файл");
		UIManager.put("FileChooser.filterLabelText", "Типы файлов");
		UIManager.put("FileChooser.detailsViewButtonToolTipText", "Содержание");
		UIManager.put("FileChooser.fileSizeHeaderText", "Размер");
		UIManager.put("FileChooser.fileDateHeaderText", "Дата изменения");
		UIManager.put("FileChooser.saveButtonToolTipText", "Сохранить");
		UIManager.put("FileChooser.saveButtonText", "Сохранить");
	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 321, 120);
		contentPane = new JPanel();
		contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		// make beautiful UI :D
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btnFileChooser = new JButton("\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u0444\u0430\u0439\u043B");
		btnFileChooser.setForeground(Color.GREEN);
		btnFileChooser.setBackground(Color.DARK_GRAY);
		btnFileChooser.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnFileChooser.setSize(105, 23);
		btnFileChooser.addActionListener(new LoadFileAction());
		btnWork = new JButton("Do Work");
		btnWork.setForeground(Color.WHITE);
		btnWork.setBackground(Color.DARK_GRAY);
		btnWork.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnWork.setBounds(106, 39, 73, 28);
		btnWork.setAlignmentX(Component.RIGHT_ALIGNMENT);
		btnWork.setEnabled(false);
		btnWork.addActionListener(new DoWorkAction());
		contentPane.setLayout(new GridLayout(2, 4, 2, 6));
		contentPane.add(btnFileChooser);
		contentPane.add(btnWork);
	}

	public Mind() {
		setTitle("IKSS Tasker v0.38 by mbv06");
		initUIText();
		initComponents();
		makeChooseColor();
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("."));
		fileChooser.resetChoosableFileFilters();
		fileChooser.setFileFilter(new FileNameExtensionFilter("Excel 1997-2003", "xls"));
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setBackground(Color.BLACK);
		fileChooser.setForeground(Color.red);
		fileChooser.updateUI();
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mind frame = new Mind();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
