package application;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import application.ReadFblocks.InvalidFileException;
import application.ReadMessages.InvalidMessageCharacteristicsException;
import application.ReadProcessorCharacteristics.InvalidProcessorCharacteristicsException;
import application.Word.InvalidElementException;
import jxl.CellView;
import jxl.Range;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * Class that combine all calculation from other files. Unfortunately, too heavy :(
 *
 */
public class Tasker {

	public final static int COLUMN_SHIFT = 19;
	public final static int ROW_SHIFT = 12;
	public final static int ROW_DATA_SHIFT = 15;
	public final static int AMOUNT_OF_REQUEST = 28;
	public final static int COLUMNS_SIZE = 5 * AMOUNT_OF_REQUEST;
	public final static String EMPTY_DESGINATION = "---";

	// Calculators :D
	private ReadIntens ri;
	private ControlOut co;
	private Workbook workbook;
	private WritableWorkbook workbook2;

	// results
	private jxl.write.Number[] resultListOpetaionCount;
	private jxl.write.Number[] resultListNumber;
	private jxl.write.Number[] resultListNumber2;
	private jxl.write.Number[] resultLambda;
	private Label[] resultQco1;
	private Label[] resultQmsgAP4;
	private Label[] resultQmsgAP3;
	private Label[] resultQmsgAP2;
	private Label[] resultQmsgAP1;
	private Label[] resultQco2;

	private WritableCellFormat wcf;
	private String list[];
	private HashMap<String, Double> map = null;
	private double summ[] = new double[6];

	public Tasker(String fileName) throws BiffException, IOException {
		workbook = Workbook.getWorkbook(new File(fileName));
		initValues();
	}

	private void initValues() {
		wcf = new WritableCellFormat();
		try {
			wcf.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
			wcf.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.MEDIUM);
			wcf.setBorder(jxl.format.Border.LEFT, jxl.format.BorderLineStyle.MEDIUM);
			wcf.setAlignment(jxl.format.Alignment.CENTRE);
			//			wcf.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
			//			wcf.setIndentation(12);
			//			wcf.set
			//			wcf.setShrinkToFit(true);
		} catch (WriteException e) {
			System.err.println("Ошибка при формировании формата ячейки");
			e.printStackTrace();
		}
	}

	public Tasker(Workbook wb) {
		workbook = wb;
		initValues();
	}

	public void calculate() throws InvalidProcessorCharacteristicsException, InvalidElementException, InvalidMessageCharacteristicsException,
			InvalidFileException, application.ReadIntens.InvalidFileException {
		readIntensity();
		calculating();
	}

	// filling empty values
	private void fillEmpty(int i, int j) {
		resultListNumber[5 * j + i] = new jxl.write.Number(COLUMN_SHIFT + 1, ROW_DATA_SHIFT + 5 * j + i, 0, wcf);
		resultListNumber2[5 * j + i] = new jxl.write.Number(COLUMN_SHIFT + 2, ROW_DATA_SHIFT + 5 * j + i, 0, wcf);
		resultLambda[5 * j + i] = new jxl.write.Number(COLUMN_SHIFT + 3, ROW_DATA_SHIFT + 5 * j + i, 0, wcf);
		resultQco1[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 4, ROW_DATA_SHIFT + 5 * j + i, "", wcf);
		resultQco2[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 5, ROW_DATA_SHIFT + 5 * j + i, "", wcf);
		resultQmsgAP1[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 6, ROW_DATA_SHIFT + 5 * j + i, "", wcf);
		resultQmsgAP2[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 7, ROW_DATA_SHIFT + 5 * j + i, "", wcf);
		resultQmsgAP3[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 8, ROW_DATA_SHIFT + 5 * j + i, "", wcf);
		resultQmsgAP4[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 9, ROW_DATA_SHIFT + 5 * j + i, "", wcf);
	}

	private void calcQco(int i, int j, double lambda) {
		resultQco1[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 4, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
		resultQco2[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 5, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
		switch (co.getNumericDestenation(5 * j + i)) {
		case 1: {
			resultQco1[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 4, ROW_DATA_SHIFT + 5 * j + i, String.valueOf(lambda * 2), wcf);
			resultQco2[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 5, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
			break;
		}
		case 2: {
			resultQco1[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 4, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
			resultQco2[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 5, ROW_DATA_SHIFT + 5 * j + i, String.valueOf(lambda * 2), wcf);
			break;
		}
		default: {
			resultQco1[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 4, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
			resultQco2[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 5, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
			break;
		}
		}
	}

	private void сalcQmsg(int i, int j, double lambda, Message[] msg) {
		resultQmsgAP1[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 6, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
		resultQmsgAP2[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 7, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
		resultQmsgAP3[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 8, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
		resultQmsgAP4[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 9, ROW_DATA_SHIFT + 5 * j + i, EMPTY_DESGINATION, wcf);
		//
		int a[] = msg[5 * j + i].getNumericDestination();

		for (int z = 0; z < a.length; z++) {
			switch (a[z]) {
			case 1: {
				resultQmsgAP1[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 6, ROW_DATA_SHIFT + 5 * j + i, String.valueOf(lambda
						* msg[5 * j + i].getMessageSize()), wcf);
				break;
			}
			case 2: {
				resultQmsgAP2[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 7, ROW_DATA_SHIFT + 5 * j + i, String.valueOf(lambda
						* msg[5 * j + i].getMessageSize()), wcf);
				break;
			}
			case 3: {
				resultQmsgAP3[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 8, ROW_DATA_SHIFT + 5 * j + i, String.valueOf(lambda
						* msg[5 * j + i].getMessageSize()), wcf);
				break;
			}
			case 4: {
				resultQmsgAP4[5 * j + i] = new jxl.write.Label(COLUMN_SHIFT + 9, ROW_DATA_SHIFT + 5 * j + i, String.valueOf(lambda
						* msg[5 * j + i].getMessageSize()), wcf);
				break;
			}
			}
		}
	}

	private void readIntensity() throws application.ReadIntens.InvalidFileException {
		ri = new ReadIntens(workbook);
		map = ri.getIntensity();
		list = ri.getReqList();
	}

	private void calcTgr(int i, int j, ArrayList<Double> result, ArrayList<Double> result2) {
		resultListNumber[5 * j + i] = new jxl.write.Number(COLUMN_SHIFT + 1, ROW_DATA_SHIFT + 5 * j + i, result.get(i) / 1000.0, wcf);
		resultListNumber2[5 * j + i] = new jxl.write.Number(COLUMN_SHIFT + 2, ROW_DATA_SHIFT + 5 * j + i, result2.get(i) / 1000.0, wcf);
	}

	private void calcOperationCount(int i, int j, double[][] logic) {
		if (i < 4) {
			resultListOpetaionCount[5 * j + i] = new jxl.write.Number(COLUMN_SHIFT, ROW_DATA_SHIFT + 5 * j + i, logic[i][j] * map.get(list[j]), wcf);
		} else {
			resultListOpetaionCount[5 * j + i] = null;
		}
	}

	private void fillResult(int i, int j, ArrayList<Double> resLambda, ArrayList<Double> result, ArrayList<Double> result2, Message[] msg) {
		calcTgr(i, j, result, result2);
		double lambda = resLambda.get(i) * map.get(list[j]);
		resultLambda[5 * j + i] = new jxl.write.Number(COLUMN_SHIFT + 3, ROW_DATA_SHIFT + 5 * j + i, lambda, wcf);
		// Qув
		calcQco(i, j, lambda);
		// Qmsg
		сalcQmsg(i, j, lambda, msg);
	}

	private void calc(Message[] msg, AI ai[], Workbook workbook, ArrayList<HashMap<String, Integer>> lisOfFBlocks) throws InvalidElementException {
		int sizeRes = 0;
		ArrayList<Double> result, result2;
		double[][] logic = new double[lisOfFBlocks.size()][AMOUNT_OF_REQUEST];
		for (int z = 0; z < lisOfFBlocks.size(); z++) {
			ai[2] = new AI(lisOfFBlocks.get(z), workbook);
			for (int q = 0; q < AMOUNT_OF_REQUEST; q++) {
				logic[z][q] = ai[2].stringEditor(3, ROW_DATA_SHIFT + q * 5);
			}
		}

		for (int j = 0; j < AMOUNT_OF_REQUEST; j++) {
			result = ai[0].newMethod(3, ROW_DATA_SHIFT + j * 5);
			result2 = ai[1].newMethod(3, ROW_DATA_SHIFT + j * 5);

			result.trimToSize();
			ArrayList<Double> resLambda = ai[0].getLambda();
			sizeRes = result.size();
			for (int i = 0; i < 5; i++) {
				calcOperationCount(i, j, logic);
				if (sizeRes <= i) {
					fillEmpty(i, j);
				} else {
					fillResult(i, j, resLambda, result, result2, msg);
				}
			}
		}
	}

	// init lists that store results
	private void initLists() {
		resultListOpetaionCount = new jxl.write.Number[5 * AMOUNT_OF_REQUEST];
		resultListNumber = new jxl.write.Number[COLUMNS_SIZE];
		resultListNumber2 = new jxl.write.Number[COLUMNS_SIZE];
		resultLambda = new jxl.write.Number[COLUMNS_SIZE];
		resultQco1 = new jxl.write.Label[COLUMNS_SIZE];
		resultQco2 = new jxl.write.Label[COLUMNS_SIZE];
		resultQmsgAP1 = new jxl.write.Label[COLUMNS_SIZE];
		resultQmsgAP2 = new jxl.write.Label[COLUMNS_SIZE];
		resultQmsgAP3 = new jxl.write.Label[COLUMNS_SIZE];
		resultQmsgAP4 = new jxl.write.Label[COLUMNS_SIZE];
	}

	private void calculating() throws InvalidProcessorCharacteristicsException, InvalidElementException, InvalidMessageCharacteristicsException,
			InvalidFileException {
		ReadProcessorCharacteristics rpc = new ReadProcessorCharacteristics(workbook, 2);
		ReadFblocks rf = new ReadFblocks(workbook, (byte) ReadFblocks.ALL_VALUES);
		ArrayList<HashMap<String, Integer>> lst = rf.getList();
		CalcFblocks cfb = new CalcFblocks(rpc.getProccessor(0), lst);
		CalcFblocks cfb2 = new CalcFblocks(rpc.getProccessor(1), lst);
		ReadMessages rm = new ReadMessages(workbook);
		Message[] msg = rm.getMessageArray();
		AI ai[] = new AI[3];

		ai[0] = new AI(cfb.getRealFblocks(), workbook, 10);
		ai[1] = new AI(cfb2.getRealFblocks(), workbook, 10);

		co = new ControlOut(workbook, AMOUNT_OF_REQUEST);
		initLists();
		calc(msg, ai, workbook, lst);
	}

	private ArrayList<Range> makeOnlyRangeOfResults(Range[] cells) {
		ArrayList<Range> listOfRange = new ArrayList<Range>();
		for (int i = 0; i < cells.length; i++) {
			if (cells[i].getTopLeft().getColumn() >= COLUMN_SHIFT) {
				listOfRange.add(cells[i]);
			}
		}
		return listOfRange;
	}

	private void cleanTheArea(WritableSheet excelSheet) throws RowsExceededException, WriteException {
		WritableCellFormat cf = new WritableCellFormat();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < COLUMNS_SIZE + 3; j++) {
				excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + i, ROW_SHIFT + j, "", cf));
			}
		}
		ArrayList<Range> listOfRange = makeOnlyRangeOfResults(excelSheet.getMergedCells());
		for (int i = 0; i < listOfRange.size(); i++) {
			excelSheet.unmergeCells(listOfRange.get(i));
		}
	}

	private void mergeSheetsForNames(WritableSheet excelSheet) throws RowsExceededException, WriteException {
		for (int i = 0; i < 4; i++) {
			excelSheet.mergeCells(COLUMN_SHIFT + i, ROW_SHIFT, COLUMN_SHIFT + i, ROW_SHIFT + 2);
		}
		excelSheet.mergeCells(COLUMN_SHIFT + 4, ROW_SHIFT, COLUMN_SHIFT + 5, ROW_SHIFT + 1);
		excelSheet.mergeCells(COLUMN_SHIFT + 6, ROW_SHIFT, COLUMN_SHIFT + 9, ROW_SHIFT + 1);
	}

	private void addOperationCount(WritableSheet excelSheet, WritableCellFormat cf) throws RowsExceededException, WriteException {
		cleanTheArea(excelSheet);
		WritableCellFormat wcf1 = new WritableCellFormat(wcf);
		wcf1.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT, ROW_SHIFT, "Кол-во операций", cf));
		for (int i = 0; i < resultListOpetaionCount.length; i++) {
			if (resultListOpetaionCount[i] == null) {
				excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT, resultListOpetaionCount[i - 1].getRow() + 1, "", wcf1));

			} else {
				excelSheet.addCell(resultListOpetaionCount[i]);
			}
		}
	}

	private void addTitles(WritableSheet excelSheet, WritableCellFormat cf) throws RowsExceededException, WriteException {
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 1, ROW_SHIFT, "Тгр(cpu1), мс.", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 2, ROW_SHIFT, "Тгр(cpu2), мс.", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 3, ROW_SHIFT, "  лямбда'  ", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 4, ROW_SHIFT, "Qув", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 4, ROW_SHIFT + 2, "ТП1", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 5, ROW_SHIFT + 2, "ТП2", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 6, ROW_SHIFT, "Qсообщ", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 6, ROW_SHIFT + 2, "АП1", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 7, ROW_SHIFT + 2, "АП2", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 8, ROW_SHIFT + 2, "АП3", cf));
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 9, ROW_SHIFT + 2, "АП4", cf));
	}

	private void addOthersValues(WritableSheet excelSheet, WritableCellFormat cf) throws RowsExceededException, WriteException {

		addTitles(excelSheet, cf);

		WritableCellFormat wcf1 = new WritableCellFormat(wcf);
		WritableCellFormat wcf2; // for empty cell
		wcf1.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
		for (int i = 0; i < resultListNumber.length; i++) {
			//making bottom border at end of request
			if ((i + 1) % 5 == 0 & resultListNumber[i].getValue() != 0) {
				resultListNumber[i].setCellFormat(wcf1);
				resultLambda[i].setCellFormat(wcf1);
				resultListNumber2[i].setCellFormat(wcf1);
				resultQco1[i].setCellFormat(wcf1);
				resultQco2[i].setCellFormat(wcf1);
				resultQmsgAP1[i].setCellFormat(wcf1);
				resultQmsgAP2[i].setCellFormat(wcf1);
				resultQmsgAP3[i].setCellFormat(wcf1);
				resultQmsgAP4[i].setCellFormat(wcf1);
			}
			if (resultListNumber[i].getValue() != 0) {
				excelSheet.addCell(resultListNumber[i]);
				excelSheet.addCell(resultLambda[i]);
				excelSheet.addCell(resultListNumber2[i]);
				excelSheet.addCell(resultQco1[i]);
				excelSheet.addCell(resultQco2[i]);
				excelSheet.addCell(resultQmsgAP1[i]);
				excelSheet.addCell(resultQmsgAP2[i]);
				excelSheet.addCell(resultQmsgAP3[i]);
				excelSheet.addCell(resultQmsgAP4[i]);
			} else {
				// empty Cell
				if ((i + 1) % 5 == 0) {
					//make bottom MEDIUM border at end of request
					wcf2 = wcf1;
				} else {
					//make bottom NORMAL border at center of request
					wcf2 = wcf;
				}
				for (int j = 0; j < 9; j++) {
					// draw whole request line
					excelSheet.addCell(new Label(COLUMN_SHIFT + j + 1, ROW_DATA_SHIFT + i, "", wcf2));
				}
			}
		}
	}

	private void doAutosize(WritableSheet excelSheet) {
		CellView cv;
		for (int i = COLUMN_SHIFT; i < COLUMN_SHIFT + 10; i++) {
			cv = excelSheet.getColumnView(i);
			cv.setAutosize(true);
			excelSheet.setColumnView(i, cv);
		}
	}

	private void calcSum() {
		summ[0] = getSummOfArray(resultQco1);
		summ[1] = getSummOfArray(resultQco2);
		summ[2] = getSummOfArray(resultQmsgAP1);
		summ[3] = getSummOfArray(resultQmsgAP2);
		summ[4] = getSummOfArray(resultQmsgAP3);
		summ[5] = getSummOfArray(resultQmsgAP4);

	}

	private double getSummOfArray(jxl.write.Label[] array) {

		double result = 0;
		String tmp;
		for (int i = 0; i < array.length; i++) {
			tmp = array[i].getContents();
			if (!tmp.equalsIgnoreCase(EMPTY_DESGINATION) && tmp.length() > 10) {
				result += Double.valueOf(array[i].getContents());
			}
		}
		return result;
	}

	private void addSumm(WritableSheet excelSheet, WritableCellFormat cf) throws RowsExceededException, WriteException {
		excelSheet.addCell(new jxl.write.Label(COLUMN_SHIFT + 3, ROW_DATA_SHIFT + COLUMNS_SIZE, "Сумма", cf));
		for (int i = 0; i < summ.length; i++) {
			excelSheet.addCell(new jxl.write.Number(COLUMN_SHIFT + i + 4, ROW_DATA_SHIFT + COLUMNS_SIZE, summ[i], cf));
		}

	}

	public void SaveTofile(String fileSaveName) throws IOException, RowsExceededException, WriteException {
		workbook2 = Workbook.createWorkbook(new File(fileSaveName), workbook);
		WritableSheet excelSheet = workbook2.getSheet(2);
		WritableCellFormat cf = new WritableCellFormat();
		cf.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.MEDIUM);
		cf.setAlignment(jxl.format.Alignment.CENTRE);
		cf.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
		addOperationCount(excelSheet, cf);
		addOthersValues(excelSheet, cf);
		calcSum();
		addSumm(excelSheet, cf);
		mergeSheetsForNames(excelSheet);
		doAutosize(excelSheet);
		workbook2.write();
		workbook2.close();
	}

	public void close() {
		workbook.close();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Tasker tsk = new Tasker("..\\XX.xls");
			tsk.calculate();
			tsk.SaveTofile("..\\XXZ.xls");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

}
