package application;

import java.util.HashMap;

public class SearchedValue {
	private String element;
	private int count;
	private HashMap<String, Integer> values;
	private String[] names;

	public SearchedValue(String element) {
		this(element, 0);
	}

	/**
	 * 
	 * @param element
	 *            - Word.element
	 * @param count
	 *            - position at chains
	 */
	public SearchedValue(String element, int count) {
		this.setElement(element);
		this.setCount(count);
		names = new String[element.length()];
		makeNames();
	}

	private void makeNames() {
		for (int i = 0; i < this.getElement().length(); i++) {
			names[i] = this.getElement().substring(0, i + 1);
		}
	}

	public String[] getNames() {
		return names;
	}

	public String getNearValue(String value) {
		if (value.length() <= names.length) {
			return names[value.length() - 1];
		} else {
			return names[names.length - 1];
		}
		// return "B";
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public HashMap<String, Integer> getValues() {
		return values;
	}

	public void setValues(HashMap<String, Integer> values) {
		this.values = values;
	}

	public String getElement() {
		return element;
	}

	public void setElement(String element) {
		this.element = element;
	}

	public static void main(String args[]) {
		SearchedValue sv = new SearchedValue("B000");
		System.out.println(sv.getNearValue("B0100"));
		String[] names = sv.getNames();
		for (int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}
	}

}
