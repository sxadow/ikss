package application;

import java.io.File;
import java.util.ArrayList;
import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;

public class ReadProcessorCharacteristics {
	public static class InvalidProcessorCharacteristicsException extends Exception {
		public InvalidProcessorCharacteristicsException(String string) {
			super(string);
		}
	}

	public final int x;
	public final int y;
	public final int processorCount;
	private ArrayList<Processor> cpus;
	private Workbook source;
	private Sheet sheet;

	/** See also {@link #ReadProcessorCharacteristics(Workbook, int, int, int)} */
	public ReadProcessorCharacteristics(Workbook wb, int processorCount) {
		source = wb;
		sheet = source.getSheet(2);
		this.processorCount = processorCount;
		cpus = new ArrayList<>(this.processorCount);
		x = 13;
		y = 2;
	}

	/**
	 * 
	 * @param wb
	 *            source file
	 * @param processorCount
	 *            number of CPU
	 * @param x
	 *            default is 13
	 * @param y
	 *            default is 2
	 */
	public ReadProcessorCharacteristics(Workbook wb, int processorCount, int x, int y) {
		source = wb;
		sheet = source.getSheet(2);
		this.processorCount = processorCount;
		cpus = new ArrayList<>(this.processorCount);
		this.x = x;
		this.y = y;
	}

	/**
	 * Get Processor
	 * 
	 * @param number
	 *            - number of read processors
	 * @return Processor
	 * @throws Exception
	 *             number is not valid
	 */
	public Processor getProccessor(int number) throws InvalidProcessorCharacteristicsException {
		if (number > processorCount) {
			throw new IllegalArgumentException("Processor doesn't exist");
		} else {
			int[] params = readData(number);
			return new Processor(params[0], params[1], params[2], params[3], params[4]);
		}
	}

	private int[] readData(int shift) throws InvalidProcessorCharacteristicsException {
		int data[] = new int[5];
		int cycles = 0;
		for (int i = 0; i < 5; i++) {
			Cell c = sheet.getCell(x + shift, y + i);
			if (c.getType() == CellType.NUMBER) {
				cycles = Integer.valueOf(c.getContents());
				if (cycles > 0) {
					data[i] = cycles;
				} else {
					throw new InvalidProcessorCharacteristicsException("Количество тактов должно быть больше 0");
				}
			} else {
				throw new InvalidProcessorCharacteristicsException("Неверные характеристики процессора");
			}
		}
		return data;
	}

	/**
	 * 
	 * get CPU list with all processors
	 */
	public ArrayList<Processor> getCPUs() {
		return cpus;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Workbook workbook;
		try {
			workbook = Workbook.getWorkbook(new File("..\\XX.xls"));
			ReadProcessorCharacteristics rdc = new ReadProcessorCharacteristics(workbook, 1);
			for (int a : rdc.readData(0)) {
				System.out.println(a);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
