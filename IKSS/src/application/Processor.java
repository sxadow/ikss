package application;

public class Processor {

	int frequency;
	public final int cycles[] = new int[4];
	private double t;
	private double tau[] = new double[4];

	/**
	 * Parameters must be greater than 0
	 */
	public Processor(int add, int mul, int div, int subb, int freq) {
		if (add < 1 || mul < 1 || div < 1 || subb < 1 || freq < 1) {
			throw new IllegalArgumentException("Values must be greater than 0");
		}
		frequency = freq;
		cycles[0] = add;
		cycles[1] = mul;
		cycles[2] = div;
		cycles[3] = subb;

		calculate();
	}

	private void calculateTau() {
		for (int i = 0; i < cycles.length; i++) {
			tau[i] = t * cycles[i];
		}
	}

	private void calculate() {
		t = 1.0 / frequency;
		calculateTau();
	}

	/**
	 * Get frequency of Processor
	 * 
	 * @return frequency*10^6
	 */
	public int getFrequencyInHz() {
		return frequency * 1000000;
	}

	/**
	 * Get frequency of Processor
	 * 
	 * @return Frequency in MHz
	 */
	public int getFrequency() {
		return frequency;
	}

	/**
	 * Get Tau
	 * 
	 * @return Tau in mS
	 */
	public double[] getTau() {
		return tau;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Processor cpu = new Processor(3, 25, 162, 9, 520);
		double[] tau = cpu.getTau();
		for (int i = 0; i < tau.length; i++) {
			System.out.println(tau[i]);
		}
	}

}
