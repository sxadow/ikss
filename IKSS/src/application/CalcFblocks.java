package application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import jxl.Workbook;

/**
 * getting FBlocks that depends on CPU
 */
public class CalcFblocks {
	private LinkedHashMap<String, Double> realFblocks;
	// ArrayList<HashMap<String, Integer>> fblocks;
	public final String names[] = { "Ф1", "Ф2", "Ф3", "Ф4", "Ф5", "Ф6", "Ф7", "Ф8", "Ф9", "Ф10" };
	private final int blocksCount = names.length;

	/**
	 * 
	 * @param cpu
	 *            - Processor characteristics
	 * @param fblock
	 *            - old FPB, size must be 4
	 * @throws Exception
	 *             if fblock.size != 4
	 */
	public CalcFblocks(Processor cpu, ArrayList<HashMap<String, Integer>> fblock) {
		if (fblock.size() != 4) {
			throw new IllegalArgumentException("CalcFblocks: Size must be 4");
		}
		realFblocks = new LinkedHashMap<String, Double>(blocksCount);
		calculate(fblock, cpu.getTau());
	}

	private void calculate(ArrayList<HashMap<String, Integer>> fblocks, double[] tau) {
		double tmp;
		for (int i = 0; i < 10; i++) {
			tmp = 0;
			for (int j = 0; j < 4; j++) {
				tmp += fblocks.get(j).get(names[i]) * tau[j];
			}
			realFblocks.put(names[i], tmp);
		}
	}

	/**
	 * FBlocks that depends on the CPU
	 */
	public HashMap<String, Double> getRealFblocks() {
		return realFblocks;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jxl.Workbook workbook = null;
		try {
			workbook = Workbook.getWorkbook(new java.io.File("..\\XX.xls"));
			ReadFblocks ri = new ReadFblocks(workbook, (byte) ReadFblocks.ALL_VALUES);
			CalcFblocks cfb = new CalcFblocks(new Processor(3, 25, 162, 9, 520), ri.getList());
			System.out.print(cfb.getRealFblocks().keySet() + "\n" + cfb.getRealFblocks().values());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
