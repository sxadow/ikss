package application;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import jxl.Cell;
import jxl.CellType;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadIntens {
	public static class InvalidFileException extends Exception {
		public InvalidFileException(String string) {
			super(string);
		}
	}

	private Workbook workbook;
	private Sheet sheet;
	public final int ROW = 1;
	public final int COLUMN = 15;
	public final int NUM = 28;
	String requestList[];

	HashMap<String, Double> FInt;

	ReadIntens(Workbook wb) {
		workbook = wb;
		sheet = workbook.getSheet(2);
		FInt = new HashMap<String, Double>(NUM);
		requestList = new String[NUM];
	}

	/**
	 * Get Intensity
	 * 
	 * @return HashMap with intensity for every thread request
	 * @throws Exception
	 *             if intensity is not calculated
	 */
	public HashMap<String, Double> getIntensity() throws InvalidFileException {
		return readerIntensity();
	}

	public String[] getReqList() {
		return requestList;
	}

	private HashMap<String, Double> readerIntensity() throws InvalidFileException {
		Cell tmp;
		String blockName = null;
		double block = 0;
		for (int i = 0; i < NUM; i++) {
			tmp = sheet.getCell(ROW, COLUMN + i * 5);
			blockName = tmp.getContents();
			tmp = sheet.getCell(ROW + 1, COLUMN + i * 5);
			if (tmp.getType() == CellType.NUMBER_FORMULA || tmp.getType() == CellType.NUMBER) {
				block = (double) ((NumberCell) tmp).getValue();
			} else {
				throw new InvalidFileException("Ошибка чтения интенсивностей! строка " + (COLUMN + i + 1) + "!");
			}
			requestList[i] = new String(blockName);
			FInt.put(blockName, block);
		}
		return FInt;
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws BiffException
	 */
	public static void main(String[] args) throws BiffException, IOException {
		// TODO Auto-generated method stub
		Workbook workbook = Workbook.getWorkbook(new File("..\\XX.xls"));
		ReadIntens ri = new ReadIntens(workbook);
		try {
			System.out.println(ri.readerIntensity().values());
		} catch (Exception e) {
			System.err.print("Ошибка чтения интенсивностей");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		workbook.close();
	}

}
