package application;

import java.util.ArrayList;
import java.util.HashMap;
import jxl.Cell;
import jxl.CellType;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;

public class ReadFblocks {
	public static class InvalidFileException extends Exception {
		public InvalidFileException(String string) {
			super(string);
		}
	}

	public final static int OPERATIONS = 4;
	public final static int ROW = 2;
	public final static int COLUMN_BLOCK = 1;
	public final static int ALL_VALUES = 15;
	private HashMap<String, Integer> fBlocksOfAddition;
	private HashMap<String, Integer> fBlocksOfMultiplication;
	private HashMap<String, Integer> fBlocksOfDivision;
	private HashMap<String, Integer> fBlocksOfTransfer;
	private final Workbook workbook;
	private final Sheet sheet;
	private final byte value;
	private ArrayList<HashMap<String, Integer>> list;

	/**
	 * @param wb
	 *            source file
	 * @param value
	 *            it indicates what operations will calculate 0001b - add 0010b
	 *            - multiply 0100b - divide 1000b - sending operation must be
	 *            greater than 0
	 * @throws Exception
	 *             when FPB is wrong
	 */
	public ReadFblocks(Workbook wb, byte value) throws InvalidFileException {
		workbook = wb;
		sheet = workbook.getSheet(2);
		list = new ArrayList<>(OPERATIONS);
		if (value > 0) {
			this.value = value;
		} else {
			throw new IllegalArgumentException("Value must be greater than 0");
		}
		readFblocks(this.value);
	}

	public HashMap<String, Integer> getFBlocksOfAddition() {
		return fBlocksOfAddition;
	}

	public HashMap<String, Integer> getFBlocksOfMultiplication() {
		return fBlocksOfMultiplication;
	}

	public HashMap<String, Integer> getFBlocksOfDivision() {
		return fBlocksOfDivision;
	}

	public HashMap<String, Integer> getFBlocksOfTransfer() {
		return fBlocksOfTransfer;
	}

	public ArrayList<HashMap<String, Integer>> getList() {
		return list;
	}

	private HashMap<String, Integer> readerBlocks(int COLUMNValue) throws InvalidFileException {
		HashMap<String, Integer> FMap = new HashMap<String, Integer>(10);
		Cell tmp;
		String blockName = null;
		int block = 0;
		for (int i = 0; i < 10; i++) {
			tmp = sheet.getCell(ROW + i, COLUMN_BLOCK);
			blockName = tmp.getContents();
			tmp = sheet.getCell(ROW + i, COLUMNValue);
			if (tmp.getType() == CellType.NUMBER) {
				block = (int) ((NumberCell) tmp).getValue();
			} else {
				throw new InvalidFileException("Ошибка чтения ФПБ");
			}
			FMap.put(blockName, block);
		}
		return FMap;
	}

	private void readFblocks(byte value) throws InvalidFileException {
		if ((value & 1) == 1) {
			fBlocksOfAddition = readerBlocks(2);
			list.add(getFBlocksOfAddition());
		}
		if ((value & 2) == 2) {
			fBlocksOfMultiplication = readerBlocks(3);
			list.add(fBlocksOfMultiplication);
		}
		if ((value & 4) == 4) {
			fBlocksOfDivision = readerBlocks(4);
			list.add(fBlocksOfDivision);
		}
		if ((value & 8) == 8) {
			fBlocksOfTransfer = readerBlocks(5);
			list.add(fBlocksOfTransfer);
		}
	}

	public static void main(String[] args) {
		Workbook workbook;
		try {
			workbook = Workbook.getWorkbook(new java.io.File("..\\XX.xls"));
			ReadFblocks rf = new ReadFblocks(workbook, (byte) ReadFblocks.ALL_VALUES);
			ArrayList<HashMap<String, Integer>> a = rf.getList();
			System.out.println(a.get(0).keySet());
			for (int i = 0; i < a.size(); i++) {
				System.out.println(a.get(i).values());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
