package application;

import java.util.ArrayList;
import java.util.HashMap;
import application.Word.InvalidElementException;
import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;

public class AI {
	private Workbook workbook;
	private final int CHAINS;
	private Sheet sheet;
	private Word word;
	private AIplus aip;
	private HashMap<String, Double> elements;
	private HashMap<String, ? extends Number> fBlocks;

	private double formatDouble(double d, int dz) {
		double dd = Math.pow(10, dz);
		return Math.round(d * dd) / dd;
	}

	public AI(HashMap<String, ? extends Number> blocks, Workbook wb) {
		workbook = wb;
		sheet = workbook.getSheet(2);
		fBlocks = blocks;
		CHAINS = 10;
	}

	public AI(HashMap<String, ? extends Number> blocks, Workbook wb, int chains) {
		workbook = wb;
		sheet = workbook.getSheet(2);
		fBlocks = blocks;
		CHAINS = chains;
	}

	// B00 getting chains B=>B0/B1
	private ArrayList<String> readChains(int row, int column) {
		Cell tmp;
		String chain;
		ArrayList<String> chainList = new ArrayList<String>(11);
		for (int i = 0; i < 5; i++) {
			tmp = sheet.getCell(row, column + i);
			if (tmp.getType() == CellType.EMPTY) {
				// nothing
			} else {
				chain = tmp.getContents();
				chainList.add(chain);
			}
		}
		return chainList;
	}

	//getting ALL chains before that and makes from it coefficient,	afterwards multiply it on his element
	private double findSum(Word chain) {
		double sum = chain.getWorkload();
		Number valueOfelement;
		String tmp = chain.getElement();
		int length = chain.getElement().length();
		//detect is this element has kids
		if (length > 2) {
			for (int i = 2; i < length; i++) {
				tmp = chain.getElement().substring(0, tmp.length() - 1); //getting previous chain BXX => BX
				sum *= elements.get(tmp); // get coefficient for previous chain
				sum = formatDouble(sum, 9); //rounding
			}
		}
		valueOfelement = fBlocks.get(chain.getFblock());
		sum *= valueOfelement.doubleValue();
		return sum;
	}

	/**
	 * Method that calculates operationCount
	 * 
	 * @param row
	 *            always starts from 3
	 * @param column
	 *            for first chain it 15
	 * @return operation count of these chains
	 * @throws InvalidElementException
	 * @throws Exception
	 */
	public double stringEditor(int row, int column) throws InvalidElementException {
		double summa = 0;
		ArrayList<String> tmp;
		elements = new HashMap<String, Double>();
		for (int i = 0; i < CHAINS; i++) {
			tmp = readChains(row + i, column);
			for (int j = 0; j < tmp.size(); j++) {
				word = new Word(tmp.get(j));

				// fixing bugs in source file
				if (word.getFblock().equals("Ф0")) {
					word.setFblock("Ф10");
				}

				if (elements.containsKey(word.getElement()) && word.getWorkload() == 1.0) {
					// replacing old coefficient of element
					word.setWorkload(elements.get(word.getElement()));
				}
				elements.put(word.getElement(), word.getWorkload()); //adding element to map
				summa += findSum(word); // calculating operation count
			}
		}
		return summa;
	}

	/**
	 * Get results
	 * 
	 * @param row
	 *            always starts from 3
	 * @param column
	 *            for first chain it 15
	 * @return the map of result for every out
	 * @throws Word.InvalidElementException
	 * @throws Exception
	 */
	public ArrayList<Double> newMethod(int row, int column) throws InvalidElementException {
		aip = new AIplus(row, column);
		return aip.reverseSearch();
	}

	public ArrayList<Double> getLambda() {
		return aip.lambdaArray;
	}

	/*
	 * private class that use to calculating new variables
	 */
	private class AIplus {
		private int row;
		private int column;
		private ArrayList<Double> lambdaArray;

		// methods that use to calculate lambda
		private double stringEditor2(SearchedValue sv) throws InvalidElementException {
			double summa = 0;
			ArrayList<String> tmp;
			elements = new HashMap<String, Double>();
			for (int i = 0; i < CHAINS; i++) {
				tmp = readChains(row + i, column);
				for (int j = 0; j < tmp.size(); j++) {
					word = new Word(tmp.get(j));
					// fix bugs from source file
					if (word.getFblock().equals("Ф0")) {
						word.setFblock("Ф10");
					}
					if (elements.containsKey(word.getElement()) && word.getWorkload() == 1.0) {
						// replacing old coefficient of element
						word.setWorkload(elements.get(word.getElement()));
					}
					elements.put(word.getElement(), word.getWorkload());
					if (sv.getValues().containsKey(word.getElement())) {
						// checking is that element contains in previous elements of searchedValue
						if (sv.getValues().get(word.getElement()) >= i) {
							summa += findSum(word);
						}
					} else {
						String element = word.getElement();
						// checking is that element using to making some element of previous elements of searchedValue 
						element = Word.getPublicValue(element, sv.getNearValue(element));
						if (sv.getValues().get(element) > i) {
							summa += findSum(word);
						}
					}
				}
			}
			lambdaArray.add(getLambda(sv.getElement(), elements));
			return summa;
		}

		//get lamda for this chain, using block(previous values)
		private double getLambda(String chain, HashMap<String, Double> blocks) {
			double sum = 1;
			String tmp = chain;
			int length = chain.length();
			if (length > 2) {
				for (int i = 2; i < length; i++) {
					tmp = chain.substring(0, tmp.length() - 1);
					sum *= elements.get(tmp);
				}
			}
			sum *= ((HashMap<String, Double>) blocks).get(chain);
			return sum;
		}

		public AIplus(int row, int column) {
			this.row = row;
			this.column = column;
			lambdaArray = new ArrayList<Double>(5);
		}

		// getting coefficient which includes previous elements
		/*private double getCoef(Word chain) {
			double sum = chain.getWorkload();
			String tmp = chain.getElement();
			int length = chain.getElement().length();
			if (length > 2) {
				for (int i = 2; i < length; i++) {
					tmp = chain.getElement().substring(0, tmp.length() - 1);
					sum *= elements.get(tmp);
					sum = formatDouble(sum, 9);
				}
			}
			return sum;
		}*/

		// detect is this value contains in steps of searchedValue(block)
		private boolean hasNeedValue(ArrayList<String> block, String value) throws InvalidElementException {
			for (int i = 0; i < block.size(); i++) {
				String element = new Word(block.get(i)).getElement();
				if (element.equals(value)) {
					return true;
				}
			}
			return false;
		}

		// adding all variants of naming this value 
		private HashMap<String, Integer> createListOfBlocks(SearchedValue sv) {
			String[] names = sv.getNames();
			HashMap<String, Integer> list = new HashMap<String, Integer>(names.length);
			for (int i = 0; i < names.length; i++)
				list.put(names[i], 0);
			return list;
		}

		private ArrayList<Double> reverseSearch() throws InvalidElementException {
			int lastRow = CHAINS + row;
			ArrayList<String> lastBlock;
			elements = new HashMap<String, Double>();
			lastBlock = readChains(lastRow - 1, column);
			HashMap<String, Integer> list;
			ArrayList<Double> result1 = new ArrayList<Double>(lastBlock.size());
			for (int j = 0; j < lastBlock.size(); j++) {
				word = new Word(lastBlock.get(j));
				SearchedValue sv = new SearchedValue(word.getElement());

				// detecting all previous values of this element
				list = createListOfBlocks(sv);
				String[] names = sv.getNames();

				//adding coordinate of previous values
				for (int i = 0; i < CHAINS; i++) {
					for (int g = 0; g < names.length; g++) {
						if (hasNeedValue(readChains(row + i, column), names[g])) {
							list.put(names[g], i);
						}
					}
				}
				sv.setValues(list); // adding previous values to sv
				result1.add(stringEditor2(sv));
			}
			return result1;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Workbook workbook;
		try {
			//			workbook = Workbook.getWorkbook(new java.io.File("..\\XX.xls"));
			workbook = Workbook.getWorkbook(new java.io.File("D:\\1.xls"));
			ReadFblocks rf = new ReadFblocks(workbook, (byte) 15);

			AI asd = new AI(rf.getFBlocksOfAddition(), workbook, 10);
			System.out.println(asd.newMethod(3, 15));
			System.out.println();
			workbook.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
