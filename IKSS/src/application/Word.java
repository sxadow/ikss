package application;

/**
 * it's word for example "B011(Ф9;86%)"
 * 
 */
public class Word {
	public static class InvalidElementException extends Exception {
		public InvalidElementException(String string) {
			super(string);
		}
	}

	public char[] allowElement = { '0', '1', '2', '3' };
	public char startSymbol = 'B';
	private String element = "None", fblock = "None";
	private double workload;

	public String getElement() {
		return element;
	}

	public String getFblock() {
		return fblock;
	}

	public double getWorkload() {
		return workload;
	}

	/**
	 * @param value
	 *            must be greater than 0, but less than 1.0
	 */
	public void setWorkload(double value) {
		if (value > 0 && value <= 1.0) {
			workload = value;
		}
	}

	public void setElement(String element) throws InvalidElementException {
		if (checkElement(element)) {
			this.element = element;
		} else {
			throw new InvalidElementException(element
					+ " is not valid, supports only '0' and '1'");
		}
	}

	public void setFblock(String block) {
		fblock = block;
	}

	private boolean isValidSymbol(char symbol) {
		for (int i = 0; i < allowElement.length; i++) {
			if (allowElement[i] == symbol) {
				return true;
			}
		}
		return false;
	}

	private boolean checkElement(String element) {
		if (!element.startsWith(String.valueOf(startSymbol))) {
			return false;
		}
		for (int i = 1; i < element.length(); i++) {
			if (!isValidSymbol(element.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	private int[] getPositionOfElementsInChain(String chain) {
		int x = 0, y = 0, z = 0;
		x = chain.lastIndexOf('(');
		y = chain.lastIndexOf(';', x);
		z = chain.lastIndexOf(')', z);
		if (chain.startsWith(String.valueOf(startSymbol)) && x > 0 && y > x
				&& z > y && z < y + 4 && z == chain.length() - 1) {
			return new int[] { x, y, z };
		} else {
			return new int[] { -1, -1, -1 };
		}
	}

	/**
	 * @param chain
	 *            string with next format "B011(Ф9;86%)"
	 * @throws InvalidElementException
	 */
	public Word(String chain) throws InvalidElementException {
		int fblockStart = 0, workloadStart = 0;
		// boolean isFoundElement = false, isFoundFblock = false,
		// isFoundWorkload = false;
		// parsing string to need variables
		for (int i = 0; i < chain.length(); i++) {
			switch (chain.charAt(i)) {
			case '(': {
				setElement(chain.substring(0, i));
				fblockStart = i + 1;
				break;
			}
			case ';': {
				fblock = chain.substring(fblockStart, i);
				workloadStart = i + 1;
				break;
			}
			case '-': {
				workload = 1;
				i = chain.length();
				break;
			}
			case ')': {
				workload = Double.valueOf(chain.substring(workloadStart, i - 1)) / 100;
				break;
			}
			}
		}
	}

	/*
	 * @return Difference between Words. Example for "B010" and "B0113" it's 2
	 */
	public static int getNumericWordDifference(Word w1, Word w2) {
		return getPublicValueResult(w1.element, w2.element);
	}

	/**
	 * See also the method {@link #getPublicValue(String, String)}.
	 */
	private static int getPublicValueResult(String s1, String s2) {
		int firstLength = s1.length();
		int secondLength = s2.length();
		if (firstLength == 1 || secondLength == 1) {
			return 0;
		}
		int minLength = firstLength > secondLength ? secondLength : firstLength;
		String temp0 = s1.substring(1, minLength);
		String temp1 = s2.substring(1, minLength);
		for (int i = 0; i < temp0.length(); i++) {
			if (temp0.charAt(i) != temp1.charAt(i)) {
				return temp0.length() - i;
			}
		}
		return 0;
		// int result = Integer.valueOf(temp0) ^ Integer.valueOf(temp1);
		// return result;
	}

	/**
	 * See also the method {@link #getPublicValue(String, String)}.
	 */
	public static String getPublicValue(Word w1, Word w2) {
		return getPublicValue(w1.element, w2.element);
	}

	/**
	 * @return element which included in first and second word
	 */
	public static String getPublicValue(String s1, String s2) {
		int firstLength = s1.length();
		int secondLength = s2.length();
		int minLength = firstLength > secondLength ? secondLength : firstLength;
		int tmp = getPublicValueResult(s1, s2);
		int subb = 0;
		if (tmp != 0) {
			// subb = String.valueOf(tmp).length();
			subb = tmp;
		}
		return s1.substring(0, minLength - subb);
	}

	/**
	 * Detect is element contains in Word.element
	 */
	public boolean isWrongWay(String elem) {
		if (getPublicValueResult(this.element, elem) == 0) {
			return false;
		} else {
			return true;
		}
	}

	// public int getIntValueOfElement() {
	// return 0;
	// }

	/**
	 * @return "Element:" + element + "\nBlock name=" + fblock + "\nWorload:" +
	 *         workload;
	 */
	public String toString() {
		return "Element:" + element + "\nBlock name=" + fblock + "\nWorload:"
				+ workload;
	}

	public static void main(String args[]) {
		Word w1;
		try {
			w1 = new Word("B0123(Ф9;86%)");
			Word w2 = new Word("B0120(Ф9;86%)");
			System.out.println(w1.getNumericWordDifference(w1, w2));
			System.out.println(Word.getPublicValue(w1, w2));
			System.out.println(w1.fblock);
			System.out.print(w1.isWrongWay(new String("B01(1(Ф9;86%)")));
		} catch (InvalidElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}