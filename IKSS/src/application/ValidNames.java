package application;

public class ValidNames {
	/** Valid names of FPBs */
	public final static String names[] = { "Ф1", "Ф2", "Ф3", "Ф4", "Ф5", "Ф6", "Ф7", "Ф8", "Ф9", "Ф10" };
	/** Valid names of Objects */
	public final static String objectNames[] = { "ТП1", "ТП2", "АП1", "АП2", "АП3", "АП4" };

	public static boolean isValidObjectName(String s) {
		for (int i = 0; i < objectNames.length; i++) {
			if (s.equals(objectNames[i])) {
				return true;
			}
		}
		return false;
	}
}
