package application;

import java.io.File;
import java.io.IOException;
import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadMessages {
	public static class InvalidMessageCharacteristicsException extends Exception {
		public InvalidMessageCharacteristicsException(String string) {
			super(string);
		}
	}

	private Sheet source;
	private final int x;
	private final int y;
	private int size = 5 * 28;
	private Message msg[] = new Message[size];

	/** See also {@link #ReadMessages(Workbook, integer, integer)} */
	public ReadMessages(Workbook src) throws InvalidMessageCharacteristicsException {
		this(src, 15, 15);
	}

	public Message[] getMessageArray() {
		return msg;
	}

	/**
	 * 
	 * @param src
	 *            source file
	 * @param x
	 *            default is 15
	 * @param y
	 *            default is 15
	 * @throws Exception
	 *             when message characteristics in source file is incorrect
	 */
	public ReadMessages(Workbook src, int x, int y) throws InvalidMessageCharacteristicsException {
		source = src.getSheet(2);
		this.x = x;
		this.y = y;
		fillArray();
	}

	private void fillArray() throws InvalidMessageCharacteristicsException {
		Cell tmp;
		String value = null;
		int size = 0;
		for (int i = 0; i < this.size; i++) {
			tmp = source.getCell(x, y + i);
			value = tmp.getContents();
			tmp = source.getCell(x + 1, y + i);
			if (tmp.getType() == CellType.NUMBER) {
				size = Integer.valueOf(tmp.getContents());
				if (size < 1) {
					throw new InvalidMessageCharacteristicsException("Error reading size of message on " + (x + 2) + " " + (y + i + 1)
							+ "\n Value must be greater than ZERO");
				} else if (value.length() == 0) {
					throw new InvalidMessageCharacteristicsException("Error reading message destenation on " + (x + 1) + " " + (y + i + 1));
				}
				try {
					msg[i] = new Message(size, value);
				} catch (Exception e) {
					throw new InvalidMessageCharacteristicsException("Error reading message characteristics on " + (y + i + 1) + " line \n"
							+ e.getLocalizedMessage());
				}
			} else if (value.length() > 0) {
				throw new InvalidMessageCharacteristicsException("Error reading size of message on " + (x + 2) + " " + (y + i + 1)
						+ "\n Value must be greater than ZERO");
			}
			size = 0;
		}
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws InvalidMessageCharacteristicsException {
		// TODO Auto-generated method stub
		try {
			Workbook workbook = Workbook.getWorkbook(new File("..\\XX.xls"));
			ReadMessages rmd = new ReadMessages(workbook);
			Message[] msd = rmd.getMessageArray();
			for (int i = 0; i < msd.length; i++) {
				if (msd[i] != null) {
					System.out.println("Obj" + i + "\nSize= " + msd[i].getMessageSize() + " Dest");
					for (String s : msd[i].getDestination()) {
						System.out.print(s + " ");
					}
					System.out.println();
				}
			}
		} catch (BiffException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
