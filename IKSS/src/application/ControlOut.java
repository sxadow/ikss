package application;

import java.io.File;
import java.io.IOException;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ControlOut {

	public final int y;
	public final int x;
	private Sheet source;
	private int size;
	private boolean[] isOut;
	private String[] destination = new String[5 * 28];

	/**
	 * x=14; y=15;
	 * 
	 * @param wb
	 * @param size
	 */
	public ControlOut(Workbook wb, int size) {
		this(wb, size, 14, 15);
	}

	public ControlOut(Workbook wb, int size, int x, int y) {
		this.size = size;
		this.y = y;
		this.x = x;
		this.size = size * 5;
		source = wb.getSheet(2);
		isOut = new boolean[this.size];
		checkControlOUt();
	}

	/**
	 * is this string contains "ДА"
	 * 
	 * @param String
	 *            that indicate is it have control out
	 * @return true - it contains | false - not
	 */
	public boolean isYes(String s) {
		if (s.contains("ДА")) {
			return true;
		}
		return false;
	}
	
	/**
	 * Determine for which of objects, signal will sent
	 * @param s - destination (ТП1/ТП2)
	 * @return name of object("ТП1"/"ТП2") or nothing ("---") 
	 */
	public String getDestination(String s) {
		if (s.contains("ТП1")) {
			return "ТП1";
		} else {
			if (s.contains("ТП2")) {
				return "ТП2";
			}
		}
		return "---";
	}

	/**
	 * 
	 * @return 0 - don't have destination | 1 - ТП1 | 2 - ТП2
	 */
	public int getNumericDestenation(int num) {
		if (destination[num].contains("ТП1")) {
			return 1;
		} else {
			if (destination[num].contains("ТП2")) {
				return 2;
			}
		}
		return 0;
	}

	private void checkControlOUt() {
		Cell tmp;
		String value = null;
		int shift;
		for (int i = 0; i < this.size; i++) {
			shift = y + i;
			tmp = source.getCell(x, shift);
			value = tmp.getContents();
			isOut[i] = isYes(value);
			if (isOut[i]) {

				//if signal sends from ТП*, it goes to ТП* (not always)
				if (shift <= 54) {
					destination[i] = "ТП1";
				} else if (shift <= 94) {
					destination[i] = "ТП2";
				}

				//checking situation when signal sends from ТПx, but goes to ТПy
				if (!getDestination(value).equals("---")) {
					destination[i] = getDestination(value);
				}
			} else {
				destination[i] = getDestination(""); // if "НЕТ"
			}
		}
	}

	public String[] getDestinationArray() {
		return destination;
	}
	
	/**
	 * Getting state of destination for some request
	 * @param i - number of destination(i<size)
	 * @return false if i>size
	 */
	public boolean getOutForNum(int i) {
		if (i > size) {
			return false;
		} else {
			return isOut[i];
		}
	}

	public boolean[] getArrayOfOuts() {
		return isOut;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Workbook workbook;
		try {
			workbook = Workbook.getWorkbook(new File("..\\XX.xls"));
			ControlOut co = new ControlOut(workbook, 28);
			co.checkControlOUt();
			for (int i = 0; i < 5 * 28; i++) {
				System.out.println(co.getNumericDestenation(i));
			}
		} catch (BiffException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
